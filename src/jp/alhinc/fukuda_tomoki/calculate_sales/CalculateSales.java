﻿ package jp.alhinc.fukuda_tomoki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

 public class CalculateSales {

	 public static void main(String[] args){
		Map<String,String> branchNameMap = new HashMap<String,String>();
		Map<String,Long> resultsoutput = new HashMap<String,Long>();

		if (args.length!=1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		if(!lstReader(args[0],branchNameMap,resultsoutput,"支店","^\\d{3}$")){
			return;
		}

		File file = new File(args[0]);
		File files[] = file.listFiles();

		ArrayList<String> rcdlist = new ArrayList<String>();
		for (int i=0; i<files.length; i++) {
			if(files[i].getName().matches("^\\d{8}.rcd$")){
				rcdlist.add(files[i].getName());
			}
		}
		for(int i = 0; i<rcdlist.size()-1; i++){
			int before = Integer.parseInt(rcdlist.get(i).replace(".rcd",""));
			int after = Integer.parseInt(rcdlist.get(i+1).replace(".rcd",""));
			if(after - before !=1){
		 		System.out.println("売上ファイル名が連番になっていません");
		 		return;
		 	}
		}
		if(!rcdReader(args[0],branchNameMap,resultsoutput,rcdlist)){
			return;
		}
		if(!methodout(args[0],branchNameMap,resultsoutput)){
			return;
		}

	 }


	 public static boolean lstReader(String dirpass, Map<String,String> NameMap, Map<String,Long> results,String category,String matcher){
		BufferedReader br = null;
		try{
			File file = new File(dirpass,"branch.lst");
			if (!file.exists()){
				System.out.println(category + "定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				if(items.length!=2){
					System.out.println(category + "定義ファイルのフォーマットが不正です");
					return false;
			}
			if(!items[0].matches(matcher)){
				System.out.println(category + "定義ファイルのフォーマットが不正です");
				return false;
			}
			NameMap.put(items[0], items[1]);
			results.put(items[0],(long)0);
			}
		}
		catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
		}
		finally {
			if(br !=null){
				try {
					br.close();
				}
				catch(IOException e ){
					System.out.println("closeできませんでした");
				}
			}
		}
		return true;
	}


	 public static boolean rcdReader(String dirpass, Map<String,String> NameMap, Map<String,Long> results,ArrayList<String> filenamelist){
		BufferedReader br = null;
		try{
			for (int i=0; i<filenamelist.size(); i++) {
				File salesfile = new File(dirpass,filenamelist.get(i));
			    if(salesfile.isDirectory()){
			    	System.out.println("売上ファイル名が連番になっていません");
			    	return false;
				}

				FileReader fr = new FileReader(salesfile);
				br = new BufferedReader(fr);
				String line;
				ArrayList<String> linelist = new ArrayList<String>();
				while((line = br.readLine()) != null) {
					linelist.add(line);
				}
				if(linelist.size()!=2){
					System.out.println(salesfile.getName()+ "のフォーマットが不正です");
					return false;
				}
				if(!linelist.get(1).matches("^\\d+$")){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
				if(!NameMap.containsKey(linelist.get(0))){
					System.out.println(salesfile.getName() + "の支店コードが不正です");
					return false;
				}
				long sum = results.get(linelist.get(0)) + Long.parseLong(linelist.get(1));
				String sumstr =Long.toString(sum);
				if(sumstr.length() > 10){
					System.out.println("合計金額が10桁を超えました");
					return false;
				}
				results.put(linelist.get(0),sum);

				if(linelist.size()>2){
					System.out.println(salesfile.getName() + "のフォーマットが不正です");
					return false;
				}
			}
		}
			catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
			}
			finally {
				if(br !=null){
					try {
						br.close();
					}
					catch(IOException e ){
						System.out.println("closeできませんでした");
					}
				}
			}
			return true;
		 }


	 public static boolean methodout(String dirpass, Map<String,String> NameMap, Map<String,Long> results){
		BufferedWriter bw = null;
		try{
			File resultsfile = new File(dirpass,"branch.out");
			FileWriter fw = new FileWriter(resultsfile);
			bw = new BufferedWriter(fw);
			for(Map.Entry<String, String> e :NameMap.entrySet()) {
				bw.write(e.getKey() + "," + e.getValue() + "," + results.get(e.getKey()));
				bw.newLine();
			}
		}
		catch(IOException e){
		System.out.println("予期せぬエラーが発生しました");
		return false;
		}
		finally {
			if(bw !=null){
				try {
					bw.close();
				}
				catch(IOException e ){
					System.out.println("集計結果ファイルを作成しない");
				}
			}
		}
		return true;
	 }
}





